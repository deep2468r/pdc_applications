
# **Lorentz Force**    

<br>

---

## **Introduction**

---

To simulate multiple particles moving through electric and magnetic fields with acting Lorentz Force simultaneously. There are 8 subplots. Each subplot has it's own electric and magnetic field and two charged particles. So, there are total 16 particles.     

---


<br>
<br>
<br>


## **How to Run**

> mpirun -n 16 python particle_motion.py

User can either see the plot live or save the plot as ***mul_pm.gif*** and ***mul_pm.png***.