# 8 Subplots
# One electron and one positive ion in each subplot = Total 16 particles


import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import matplotlib.animation as animation
import numpy as np
from pyrsistent import v
from scipy.integrate import odeint
from mpi4py import MPI
import sys


# Functions

def EBfield( pos, mode ):

    X = pos[0]; Y = pos[1]; Z = pos[2]


    if mode == 0:
        Ex = 0
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1
        
    elif mode == 1:
        Ex = 1e4
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1
    elif mode == 2:
        Ex = 0
        Ey = 0
        Ez = 0

        Bx = 0
        By = 2
        Bz = 2
    elif mode == 3:
        Ex = 0
        Ey = 0
        Ez = 0

        Bx = 1
        By = 0
        Bz = 1
    elif mode == 4:
        Ex = 1e3
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1
    elif mode == 5:
        Ex = 1e2
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1
    elif mode == 6:
        Ex = 10
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1
    elif mode == 7:
        Ex = 0
        Ey = 0
        Ez = 0

        Bx = 0
        By = 0
        Bz = 1

    return Ex, Ey, Ez, Bx, By, Bz


def rk4( yold, t ):
# dummy t used for odeint calls

    k1 = dydt( yold                , t )    
    k2 = dydt( yold + 0.5*dt[i]*k1 , t )
    k3 = dydt( yold + 0.5*dt[i]*k2 , t )
    k4 = dydt( yold + dt[i]*k3     , t )

    ynew = yold + (dt[i]/6.)*( k1 + 2*k2 + 2*k3 + k4 )

    return ynew


def dydt( pv, t ):
# dummy t for odeint
# NOTE moving dfdt to main (or elsewhere) leads to unphysical damping

    dfdt = np.zeros(6)

    vx = pv[3]; vy = pv[4]; vz = pv[5]

    # dxdt
    dfdt[0] = vx
    dfdt[1] = vy
    dfdt[2] = vz

    # dv/dt from Lorentz force
    dfdt[3] =  qi[i]/mi[i] * ( Ex[ i, ti ] + (vy*Bz[ i, ti ] - vz*By[ i, ti ]) )
    dfdt[4] =  qi[i]/mi[i] * ( Ey[ i, ti ] + (vz*Bx[ i, ti ] - vx*Bz[ i, ti ]) )
    dfdt[5] =  qi[i]/mi[i] * ( Ez[ i, ti ] + (vx*By[ i, ti ] - vy*Bx[ i, ti ]) )

    return dfdt 





# MAIN LOOP
if __name__=="__main__":

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    no_of_particles = 16
    
    if size > 16:
        if rank == 0 :
            sys.exit( "\nError : No. of processes = {} not a factor of no. of particles = {}".format( size, no_of_particles ) )
        else:
            sys.exit()

    particles_per_core = no_of_particles//size
    particle_id = np.zeros( particles_per_core, dtype=int )

    # Fundamental constants
    #qi = 1.602e-19		# Coulomb
    #mi = 1.661e-27		# kg
    Ti = 10			# eV
    B0 = 1                      # when uniform background [Tesla]

    qi = np.zeros( particles_per_core, dtype=float )
    mi = np.zeros( particles_per_core, dtype=float )
    
    for i in range( particles_per_core ):
        particle_id[i] = (rank*particles_per_core) + i
        
        if particle_id[i]%2 == 0:
            # Proton
            qi[i] = 1.602e-19 
            mi[i] = 1.661e-27
        else:
            # Electron
            qi[i] = -1.602e-19 
            mi[i] = 9.109e-31*1000 
        
    
    # Plasma properties
    
    vthi = np.zeros( particles_per_core, dtype=float )
    wci = np.zeros( particles_per_core, dtype=float )
    Twci = np.zeros( particles_per_core, dtype=float )
    rhoi = np.zeros( particles_per_core, dtype=float )
    
    for i in range( particles_per_core ):    
        if qi[i] > 0:
            # Proton
            vthi[i] = np.sqrt( Ti*qi[i]/mi[i] )
        else:
            # Electron
            vthi[i] = -np.sqrt( Ti*abs(qi[i])/mi[i] )
        
        wci[i]  = qi[i]*B0/mi[i]
        Twci[i] = 2*np.pi/wci[i]
        rhoi[i] = vthi[i]/wci[i]
    
        print( "\nParticle {} :- ".format( particle_id[i] ) )
        print( 'Thermal velocity :', vthi[i] )
        print( 'Time period wci  :', Twci[i] )
        print( 'Larmor radius    :', rhoi[i] )
        print( "\n" )
        sys.stdout.flush()


    # ODEINT
    time_length = 200
    
    time = np.zeros( ( particles_per_core, time_length ), dtype=float )
    dt = np.zeros( particles_per_core, dtype=float )

    pos_vel = np.zeros( ( particles_per_core, time_length, 6 ), dtype=float )
    
    Ex = np.zeros( ( particles_per_core, time_length ), dtype=float )
    Ey = np.zeros( ( particles_per_core, time_length ), dtype=float )
    Ez = np.zeros( ( particles_per_core, time_length ), dtype=float )
    Bx = np.zeros( ( particles_per_core, time_length ), dtype=float )
    By = np.zeros( ( particles_per_core, time_length ), dtype=float )
    Bz = np.zeros( ( particles_per_core, time_length ), dtype=float )
    
    x_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    y_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    z_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    vx_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    vy_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    vz_t = np.zeros( ( particles_per_core, time_length ), dtype=float )
    
    rho = np.zeros( particles_per_core, dtype=float )
    energy = np.zeros( ( particles_per_core, time_length ), dtype=float )

    for i in range( particles_per_core ):
        time[i] = np.linspace( 0, 10*Twci[i], time_length )
        dt[i] = time[ i, 1 ]-time[ i, 0 ]

        # Initial conditions
        if particle_id[i]%2 == 0:
            # Proton
            pos_vel[ i, 0, : ] = [ 0, 0, 0, abs(vthi[i]), abs(vthi[i]), abs(vthi[i]) ]
        else:
            # Electron
            pos_vel[ i, 0, : ] = [ rhoi[i], rhoi[i], 0, abs(vthi[i]), abs(vthi[i]), abs(vthi[i]) ]
        
        # Evolve fields
        for ti in range( time_length ):

            pos = pos_vel[ i, ti, 0:3 ]

            if particle_id[i] < 2:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 0 )
            elif particle_id[i] < 4:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 1 )
            elif particle_id[i] < 6:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 2 )
            elif particle_id[i] < 8:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 3 )
            elif particle_id[i] < 10:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 4 )
            elif particle_id[i] < 12:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 5 )
            elif particle_id[i] < 14:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 6 )
            elif particle_id[i] < 16:
                Ex[ i, ti ], Ey[ i, ti ], Ez[ i, ti ], Bx[ i, ti ], By[ i, ti ], Bz[ i, ti ] = EBfield( pos, 7 )

            if ti < time_length-1:
                pos_vel[ i, ti+1, : ] = rk4( pos_vel[ i, ti, : ], 0 )

        # Built in ODEINT
        #pos_vel = odeint( dydt,posvel0, time )

        # Diagnostics
        x_t[i] = pos_vel[ i, :, 0 ]
        y_t[i] = pos_vel[ i, :, 1 ]
        z_t[i] = pos_vel[ i, :, 2 ]
        vx_t[i] = pos_vel[ i, :, 3 ]
        vy_t[i] = pos_vel[ i, :, 4 ]
        vz_t[i] = pos_vel[ i, :, 5 ]

        rho[i] = 0.5*abs( np.max( x_t[i] ) - np.min( x_t[i] ) )
        energy[i] = ( vx_t[i]**2 + ( vy_t[i] + Ex[ i, -1 ]/Bz[ i, -1 ] )**2 ) / vthi[i]**2
        print( "Particle {} rho from evolution : {}".format( particle_id[i], rho[i] ) )
        sys.stdout.flush()
    

        time[i] /= Twci[i]
    
    
    
    # Gather data from all cores
    x_t_all = np.array( comm.gather( x_t, root=0 ) )
    y_t_all = np.array( comm.gather( y_t, root=0 ) )
    z_t_all = np.array( comm.gather( z_t, root=0 ) )
    time_all = np.array( comm.gather( time, root=0 ) )
    
    # Test
    Ex_all = np.array( comm.gather( Ex, root=0 ) )
    Ey_all = np.array( comm.gather( Ey, root=0 ) )
    Ez_all = np.array( comm.gather( Ez, root=0 ) )
    Bx_all = np.array( comm.gather( Bx, root=0 ) )
    By_all = np.array( comm.gather( By, root=0 ) )
    Bz_all = np.array( comm.gather( Bz, root=0 ) )
    
    
    
    # Plotting the data
    if rank == 0:  
        x_t_all = x_t_all.reshape( 16, time_length )
        y_t_all = y_t_all.reshape( 16, time_length )
        z_t_all = z_t_all.reshape( 16, time_length )
        time_all = time_all.reshape( 16, time_length )
         
        Ex_all = Ex_all.reshape( 16, time_length )
        Ey_all = Ey_all.reshape( 16, time_length )
        Ez_all = Ez_all.reshape( 16, time_length )
        Bx_all = Bx_all.reshape( 16, time_length )
        By_all = By_all.reshape( 16, time_length )
        Bz_all = Bz_all.reshape( 16, time_length )
        
        
        
        # Plotting
        fig = plt.figure( figsize = ( 15, 9 ) )
        
        ax = list()
        
        
        no_of_fld_lines = 3
        
        fld_x_list = list()
        fld_y_list = list()
        fld_z_list = list()
        
        fld_Ex_list = list()
        fld_Ey_list = list()
        fld_Ez_list = list()
        
        fld_Bx_list = list()
        fld_By_list = list()
        fld_Bz_list = list()
                
        E_quivers = list()
        B_quivers = list()
        
        
        for i in range( no_of_particles//2 ):
            ax.append( fig.add_subplot( 2, no_of_particles//4, i+1, projection = '3d' ) )
        
            ax[i].set_xlim3d( [ np.min( x_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ), np.max( x_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ) ] )
            ax[i].set_ylim3d( [ np.min( y_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ), np.max( y_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ) ] )
            ax[i].set_zlim3d( [ np.min( z_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ), np.max( z_t_all[ (2*i):(2*(i+1)) ].reshape(-1) ) ] )
            
            ax[i].set_xlabel( r'$ x/ \rho$', size=15 )
            ax[i].set_ylabel( r'$ y/ \rho$', size=15 )
            ax[i].set_zlabel( r'$ z/ \rho$', size=15 )
            
            ax[i].set_title( "E = ( {}, {}, {} )\nB=( {}, {}, {} )".format( Ex_all[ i*2, 0 ], Ey_all[ i*2, 0 ], Ez_all[ i*2, 0 ], Bx_all[ i*2, 0 ], By_all[ i*2, 0 ], Bz_all[ i*2, 0 ] ) )
            
            
            fld_xv = np.linspace( np.min(x_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), np.max(x_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), no_of_fld_lines )
            fld_yv = np.linspace( np.min(y_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), np.max(y_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), no_of_fld_lines )
            fld_zv = np.linspace( np.min(z_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), np.max(z_t_all[ (2*i):(2*(i+1)) ].reshape(-1)), no_of_fld_lines )
            
            fld_x, fld_y, fld_z = np.meshgrid( fld_xv, fld_yv, fld_zv )
            
            fld_x_list.append( fld_x )
            fld_y_list.append( fld_y )
            fld_z_list.append( fld_z )
            
            
            
            fld_Ex_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + Ex_all[ i*2, 0 ] )
            fld_Ey_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + Ey_all[ i*2, 0 ] )
            fld_Ez_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + Ez_all[ i*2, 0 ] )
            
            fld_Bx_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + Bx_all[ i*2, 0 ] )
            fld_By_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + By_all[ i*2, 0 ] )
            fld_Bz_list.append( np.zeros( shape = ( no_of_fld_lines, no_of_fld_lines, no_of_fld_lines ) ) + Bz_all[ i*2, 0 ] )
            
            E_quiver = ax[i].quiver( fld_x_list[i], fld_y_list[i], fld_z_list[i], fld_Ex_list[i], fld_Ey_list[i], fld_Ez_list[i], color='green', length=0.00000002, arrow_length_ratio=1, linewidths=1 )
            E_quivers.append( E_quiver )
            
            B_quiver = ax[i].quiver( fld_x_list[i], fld_y_list[i], fld_z_list[i], fld_Bx_list[i], fld_By_list[i], fld_Bz_list[i], color='red', length=0.005, arrow_length_ratio=0.002, linewidths=1 )
            B_quivers.append( B_quiver )
            
        
        

        lines = list()
        particle = list()

        for i in range( no_of_particles ):
            if i%2 == 0:
                line, = ax[i//2].plot3D( [], [], [], label=i+1, color='orange' )
            else:
                line, = ax[i//2].plot3D( [], [], [], label=i+1, color='dodgerblue' )
            
            lines.append( line )
          
        
        for i in range( no_of_particles ):
            if i%2 == 0:  
                particle, = ax[i//2].plot3D( [], [], [], marker='o', color='orange' )
            else:
                particle, = ax[i//2].plot3D( [], [], [], marker='o', color='dodgerblue' )
            
            lines.append( particle )
            
            
        
        for i in range( no_of_particles//2 ):
            ax[i].legend( loc='best' )
            
            
        fld_interval = time_length//10
        flag = False
                
        def animate( tt ):
            global E_quivers
            global B_quivers
            global flag
        
            
            for i in range( no_of_particles//2 ):
                ax[i].set_title( "E = ( {}, {}, {} )\nB=( {}, {}, {} )".format( Ex_all[ i*2, tt ], Ey_all[ i*2, tt ], Ez_all[ i*2, tt ], Bx_all[ i*2, tt ], By_all[ i*2, tt ], Bz_all[ i*2, tt ] ) )
            
            for i in range( no_of_particles ):
                lines[i].set_data( x_t_all[ i, :tt+1 ], y_t_all[ i, :tt+1 ] )
                lines[i].set_3d_properties( z_t_all[ i, :tt+1 ] )

                lines[i+no_of_particles].set_data( x_t_all[ i, tt ], y_t_all[ i, tt ] )
                lines[i+no_of_particles].set_3d_properties( z_t_all[ i, tt ] )
            
            if ( tt%fld_interval == 0 and tt!=0 ) or flag:
                if not flag:
                    for i in range( no_of_particles//2 ):
                        E_quivers[i] = ax[i].quiver( fld_x_list[i], fld_y_list[i], fld_z_list[i], fld_Ex_list[i], fld_Ey_list[i], fld_Ez_list[i], color='green', length=0.00000002, arrow_length_ratio=1, linewidths=1 )
                        B_quivers[i] = ax[i].quiver( fld_x_list[i], fld_y_list[i], fld_z_list[i], fld_Bx_list[i], fld_By_list[i], fld_Bz_list[i], color='red', length=0.005, arrow_length_ratio=0.002, linewidths=1 )
                
                if tt%5 == 0:
                    flag = not flag
                
            elif tt%fld_interval == 6:
                for i in range( no_of_particles//2 ):
                    E_quivers[i].remove()
                    B_quivers[i].remove()
                        
                    
            return lines
        
        
        anim = animation.FuncAnimation( fig, animate, frames=np.arange( 0, time_length ), interval=200, blit=False, repeat=False )
        
        anim.save( 'mul_pm.gif' )
        fig.savefig( 'mul_pm.png' )
        #plt.show()
    
    