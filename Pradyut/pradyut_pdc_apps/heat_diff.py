# Heat Diffusion
# Modes :- - Basic
#          - Scale for different number of cores
#          - Visualization with an edge as a heat source/sink
# Reads input from hd_input.json file and runs accordingly

from mpi4py import MPI
import sys
import copy
import time
import json
import numpy as np
import matplotlib.pyplot as plt
from os import environ
import matplotlib.animation as animation



# Functions

# To handle matplotlib warnings
def suppress_qt_warnings():
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"

# Function to join array data of all cores
def join_arr( data, grid_size, slice ):
    arr = np.array( data[0] )
    arr = arr[ 1:grid_size+1, 1:slice+1 ]
    
    for i in range( 1, len(data) ):
        b = np.array( data[i] )
        b = b[ 1:grid_size+1, 1:slice+1 ]
        arr = np.hstack( ( arr, b ) )
    

    return arr

# Function to calculate coordinates of heat source/sink as per the grid_size
def ftoi( gs, v ):
    return int( gs*v )




# Main

suppress_qt_warnings()

# MPI variables
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# Other variables
# right => rank(process) on the right side
# left => rank(process) on the left side
# grid_size => size of the matrix( i.e. 2d plane )
# max_iter = Number of iterations of loop
# interval => Number of iterations after which data will be collected and plotted
# old_temp => 2D Old temperature data matrix to hold temperature data of previous iterations
# new_temp => 2D Newly calculated temperature data matrix
# source_temp => Temperature of heat source
# sink_temp => Temperature of heat sink
# tolerance => Temperature difference ( dt ) upto which data will be processesed
# data_arr => List to record temperature data
# data_iter_arr => List to record iteration of temperature data recorded
# time_arr => List to record time elapsed

# Flags of modes 
# m_basic => To just do calculations
# m_visual => To create a plot from the data
# m_scale => To do scaling
# happy => Whether temperature difference has lowered upto tolerance value


# Flags/Operation parameters
with open( "hd_input.json", "r" ) as in_f:
    d = json.load( in_f )

grid_size = d["grid_size"]
iterations = d["iterations"]
interval = d["interval"]
source_temp = d["source_temp"]
sink_temp = d["sink_temp"]
tolerance = d["tolerance"]
save_data = d["save_data"]


if d["mode"] == "basic":
    m_visual = False
    m_strong_scale = False
    m_weak_scale = False

elif d["mode"] == "visual":
    m_visual = True
    m_strong_scale = False
    m_weak_scale = False

elif d["mode"] == "strong_scale":
    m_visual = False
    m_strong_scale = True
    m_weak_scale = False
    
elif d["mode"] == "weak_scale":
    m_visual = False
    m_strong_scale = False
    m_weak_scale = True
            
else:
    sys.exit( "Note :- Enter valid mode" )


# Mode initialisation
if m_strong_scale:
    sc_file = "hd_core_time_strong.csv"
elif m_weak_scale:
    sc_file = "hd_core_time_weak.csv"
    grid_size = size*8

if m_visual:
    color_map = d["color_map"]
    vs_anim_file = "hd_visual_anim.avi"
    vs_final_file = "hd_visual_final.png"

result_file = "hd_result.json"

right = rank + 1
left = rank - 1
happy = False



# Root process
if rank == 0:
    if m_visual:
        print( "\nHeat Diffusion Visualization\n" )
    elif m_strong_scale:
        print( "\nHeat Diffusion Strong Scale\n" )
    elif m_weak_scale:
        print( "\nHeat Diffusion Weak Scale\n" )
    else:
        print( "\nHeat Diffusion\n" )
        
    print( "Heat source/sink = Edge" )
    
    print( "Grid size = {}".format( grid_size ) )
    print( "Iterations = {}".format( iterations ) )
    print( "Heat Source temperature = {}".format( source_temp ) )
    print( "Heat Sink temperature = {}".format( sink_temp ) )
    if m_visual:
        print( "Tolerance = {}".format( tolerance ) )
    print( "No. of processes( cores ) = {}\n".format(size) )
    sys.stdout.flush()
    
    if m_visual:
        data_arr = list()
        time_arr = list()
        data_iter_arr = list()


# Set up distributed data approach
# Check if grid size is divisible by size
if grid_size%size == 0:
    slice = int( grid_size//size )
else:
    sys.exit("Error : No. of processes = {} not a factor of grid size = {}".format( size, grid_size ))


# Initial configuration
old_temp = [ [ 0 for j in range(slice+2) ] for i in range(grid_size+2) ]
new_temp = [ [ 0 for j in range(slice+2) ] for i in range(grid_size+2) ]

# Set heat source/sink
for i in range( 1, slice+1 ):
    old_temp[1][i] = sink_temp
    old_temp[grid_size][i] = source_temp
  
  
if m_visual:
    # Gather arrays from all processes
    data = comm.gather( old_temp, root=0 )


if rank == 0:
    # Initial time
    t_i = time.time()
    
    # Record the initial data
    if m_visual:
        arr = join_arr( data, grid_size, slice )

        data_arr.append( arr )
        time_arr.append( 0 )
        data_iter_arr.append( 0 )


# Do the main calculation
for j in range( 1, iterations+1 ): 
    
    # Exchange data among processes
    if rank<size-1:
        # Send rightmost strip to right-node to be received as left halo
        s1 = [ old_temp[i][slice] for i in range( 1, grid_size+1) ]
        sreqR = comm.isend( s1, dest=right, tag=1 )
        sreqR.wait()
        
        # Recieve right halo from right-node
        reqR = comm.irecv( source=right, tag=2 )
        r2 = reqR.wait()
        
        for i in range( 1, grid_size+1 ):
            old_temp[i][slice+1] = r2[i-1]
        
    
    if rank>0:
        # Recieve left halo from left-node
        reqL = comm.irecv( source=left, tag=1 )
        r1 = reqL.wait()
        
        for i in range( 1, grid_size+1 ):
            old_temp[i][0] = r1[i-1] 
        
        # Send leftmost strip to left-node to be received as right halo
        s2 = [ old_temp[i][1] for i in range( 1, grid_size+1) ]
        sreqL = comm.isend( s2, dest=left, tag=2 )
        sreqL.wait()

        

    # Update interior points using halo from previous iteration
    for x in range( 2, slice ):
        for y in range( 1, grid_size+1 ):
            new_temp[y][x] = 0.2 * ( old_temp[y][x] + old_temp[y-1][x] + old_temp[y+1][x] + old_temp[y][x-1] + old_temp[y][x+1] )    
        

    # Update halo-dependent points
    for y in range( 1, grid_size+1 ):
        new_temp[y][1] = 0.2 * ( old_temp[y][1] + old_temp[y-1][1] + old_temp[y+1][1] + old_temp[y][1-1] + old_temp[y][1+1] )
        new_temp[y][slice] = 0.2 * ( old_temp[y][slice] + old_temp[y-1][slice] + old_temp[y+1][slice] + old_temp[y][slice-1] + old_temp[y][slice+1] )
    
    
    # Reinforce heat sink/source
    for i in range( 1, slice+1 ):
        new_temp[1][i] = sink_temp
        new_temp[grid_size][i] = source_temp
    

    if j%interval == 0:        
        if m_visual:
            # Collect array data
            data = comm.gather( new_temp, root=0 )
            
            if rank == 0:                
                # Join array data of all cores
                arr = join_arr( data, grid_size, slice )
                
                # Record temperature array data
                data_arr.append( arr )
                
                # Record elapsed time
                time_arr.append( time.time() - t_i )
                
                # Record iteration for which data is saved
                data_iter_arr.append( j )          

        
            # Tolerance check
            if rank == size//2:
                if j > iterations//grid_size:
                    temp_diff = abs( new_temp[6][1] - old_temp[6][1] )
                    
                    if temp_diff < tolerance:
                        happy = True

            else:
                happy = True
                
            all_happy = comm.allreduce( happy, op=MPI.LAND )
            
            if all_happy:              
                if rank == 0:
                    print( "Iterations completed = {}".format( j ) )
                    sys.stdout.flush()
                
                break
               
    
    # Update old <- new
    old_temp = copy.deepcopy(new_temp)  
    


# Root process
if rank == 0:
    # Final computation time elapsed
    t_f = time.time() - t_i
    
    print( "\nComputation time = {:.3f} s".format( t_f ) )
    sys.stdout.flush()
    
    
    if m_visual:
        
        # Convert collected data list to numpy array
        data_arr = np.array( data_arr )
        time_arr = np.array( time_arr )
        data_iter_arr = np.array( data_iter_arr )
        
        # Save computation time in a json file
        if save_data:
            result = dict()
            result["comp_time"] = round( t_f, 3 )
            
            with open( result_file, "w" ) as r_f:
                r_f.write( json.dumps( result, indent=4 ) )
                
                
        # Plotting the data
        fig = plt.figure( figsize = ( 5, 7 ) )
        
        ax = fig.add_subplot(111)
        ax.set_title( "Iterations = {}\nTime Elapsed = {:.1f} seconds\nTolerance = {}".format( data_iter_arr[-1], time_arr[-1], tolerance ), fontsize=17 )
        cax = ax.pcolormesh( data_arr[ -1, :, : ], cmap=color_map, vmin = sink_temp, vmax=source_temp )
        fig.colorbar( cax, label = "\u2103", orientation = 'horizontal' )
        
        
        # Animation of plot
        def animate( tt ):
            ax.set_title( "Iterations = {}\nTime Elapsed = {:.1f} seconds\nTolerance = {}".format( data_iter_arr[tt], time_arr[tt], tolerance ), fontsize=17 )
            cax.set_array( data_arr[ tt, :, : ] )
            

        #anim = animation.FuncAnimation( fig, animate, np.arange( 0, len(data_iter_arr) ), interval=20, repeat=False )
        
        
        # Save generated plot as specified file
        if save_data:
            #plt.show()
            #anim.save( vs_anim_file )
            fig.savefig( vs_final_file, bbox_inches='tight' )
        else:
            plt.show()

    elif m_strong_scale or m_weak_scale:
        print( "Iterations completed = {}".format( iterations ) )
        sys.stdout.flush()
        
        # Save the number of cores used along with its computation time and data size
        with open( sc_file, "a" ) as f:
            f.write( "{},{},{}\n".format( size, round( t_f, 3 ), grid_size ) )
    

if rank == size//2:
    print( "\nResult = {:.2f} Celsius".format( new_temp[ ftoi( grid_size, 0.75 ) ][ slice//2 ] ) )



# End    
