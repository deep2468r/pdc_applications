# Pi Estimate :- To calculate the value of Pi using Monte Carlo approach.
# Modes :- - Visualization
#          - Error rate
#          - Scale for different number of cores
# Reads input from pi_input.json file and runs accordingly

#from matplotlib import markers
from mpi4py import MPI
#import random
import math
import time
from itertools import chain
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from os import environ
import sys
import json
import numpy as np
from numpy import random


# To handle matplotlib warnings
def suppress_qt_warnings():
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"


# Function to generate points and classify them as whether they are inside or outside the circle
def gen( core_v, m_visual ):
    c_in = 0
    x = random.uniform( low=-1.0, high=1.0, size=core_v )
    y = random.uniform( low=-1.0, high=1.0, size=core_v )
    
    if m_visual:
        in_x = list()
        in_y = list()
        out_x = list()
        out_y = list()
        
    
    for i in range( core_v ):
        
        if ( x[i]**2 + y[i]**2 ) <= 1.0:
            c_in += 1
            if m_visual:
                in_x.append( x[i] )
                in_y.append( y[i] )
        else:
            if m_visual:
                out_x.append( x[i] )
                out_y.append( y[i] )
            
            
    if m_visual:
        return [ c_in, in_x, in_y, out_x, out_y ]
    else:
        return c_in





# Main

# MPI variables
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


# Variables
# total_v => Total number of points to generate
# no_of_batch => Number of batches to divide the data into
# batch_v => Number of points in each batch
# core_v => Number of points in each process( core )
# pi => Pi value
# c_in => Number of points inside the circle
# apprx_pi => Pi value being calculated from points
# error => Error percentage of calculated Pi and actual Pi value
# t_v => Total number of points generated so far
# t_c_in => Total number of points present inside the circle so far
# error_list => List to store error percentage
# v_list => List to store total number of points processed
# in_x => List of x-coordinates of points inside circle
# in_y => List of y-coordinates of points inside circle
# out_x => List of x-coordinates of points outside circle
# out_y => List of y-coordinates of points outside circle 

# Flags/Operation Parameters
with open( "pe_input.json", "r" ) as in_f:
    d = json.load( in_f )

num_points_pow_2 = d["num_points_pow_2"]
total_v = pow( 2, num_points_pow_2 )
#total_v = d["num_points"]
no_of_batch = d["no_of_batch"]
save_data = d["save_data"]

if d["mode"] == "basic":
    m_visual = False
    m_strong_scale = False
    m_weak_scale = False

    no_of_batch = 1

elif d["mode"] == "visual":
    m_visual = True
    m_strong_scale = False
    m_weak_scale = False
    
elif d["mode"] == "strong_scale":
    m_visual = False
    m_strong_scale = True
    m_weak_scale = False
        
elif d["mode"] == "weak_scale":
    m_visual = False
    m_strong_scale = False
    m_weak_scale = True
      


# Modes initialisation
if m_visual:
    #total_v = pow( 2, 18 )
    #no_of_batch = 16
    vs_anim_file = "pe_visual_anim.avi"
    vs_final_file = "pe_visual_final.png"
    
elif m_strong_scale:
    #total_v = pow( 2, 24 )
    no_of_batch = 1
    sc_file = "pe_core_time_strong.csv"
    
elif m_weak_scale:
    #total_v = size*pow( 2, 18 )
    total_v = size*total_v
    no_of_batch = 1
    sc_file = "pe_core_time_weak.csv"

result_file = "pe_result.json"



if total_v%no_of_batch == 0:
    batch_v = int( total_v//no_of_batch )
else:
    sys.exit("Error : No. of batches = {} not a factor of total points = {}".format( no_of_batch, total_v ))

if batch_v%size == 0:
    core_v = int( batch_v//size )
else:
    if m_visual:
        sys.exit("Error : No. of processes = {} not a factor of points in each batch = {}".format( size, batch_v ))
    else:
        sys.exit("Error : No. of processes = {} not a factor of total points = {}".format( size, batch_v ))



# Root core
if rank == 0:
    suppress_qt_warnings()
    
    pi = math.pi
    apprx_pi = 0.0
    error = 0.0
    t_v = 0
    t_c_in = 0
    
    if m_visual:
        print( "\nPi Estimate Visualization" )
    elif m_strong_scale:
        print( "\nPi Estimate Strong Scale" )
    elif m_weak_scale:
        print( "\nPi Estimate Weak Scale" )
    else:
        print( "\nPi Estimate" )
    
    print( "\nTotal points = {} = 2^{}".format( total_v, num_points_pow_2 ) )
    print( "No. of processes( cores ) = {}".format( size ) )
    
    if m_visual:
        print( "No. of batches = {}".format( no_of_batch ) )
        print( "Points per batch = {}".format( batch_v ) )
    
    print( "Points per process( core ) = {}\n".format( core_v ) )
    sys.stdout.flush()
    
    if m_visual:
        t_v_list = list()
        t_c_in_list = list()
        apprx_pi_list = list()
        error_list = list()
        time_list = list()
        
        in_x_all = list()
        in_y_all = list()
        out_x_all = list()
        out_y_all = list()
       
    # Initial time
    t_i = time.time()


# Loop of batches to calculate Pi
for batch in range( 1, no_of_batch+1 ):
    
    # Generating points
    if m_visual:    
        c_in, in_x, in_y, out_x, out_y = gen( core_v, m_visual )
    else:
        c_in = gen( core_v, m_visual )

    # Gather data from all cores
    c_in = comm.gather( c_in, root=0 )
    if m_visual:
        in_x = comm.gather( in_x, root=0 )
        in_y = comm.gather( in_y, root=0 )
        out_x = comm.gather( out_x, root=0 )
        out_y = comm.gather( out_y, root=0 )


    # Root core
    if rank == 0:
        # Points in circle so far
        t_c_in += sum( c_in )
        
        # Total points generated so far
        if m_visual:
            t_v += batch_v
        else:
            t_v = core_v*size
        
        # Calculate Pi
        apprx_pi = ( t_c_in/t_v )*4.0
        error = abs( apprx_pi/pi - 1 )*100.0
        
        # Create lists of data
        if m_visual:
            t_v_list.append( t_v )
            t_c_in_list.append( t_c_in )
            apprx_pi_list.append( apprx_pi )
            error_list.append( error )
            
            in_x = list( chain.from_iterable( in_x ) )
            in_y = list( chain.from_iterable( in_y ) )
            out_x = list( chain.from_iterable( out_x ) )
            out_y = list( chain.from_iterable( out_y ) )
            
            in_x_all.append( in_x )
            in_y_all.append( in_y )
            out_x_all.append( out_x )
            out_y_all.append( out_y )
            
            # Record elapsed time
            time_list.append( time.time() - t_i )
            
            print( "Batch = {}".format( batch ) )
        
        print( "Points in circle = {}".format( t_c_in ))
        print( "Points generated = {}".format( t_v ) )
        print( "Approx. Pi = {:.7f}".format( apprx_pi ) )
        print( "Error = {:.3f} percentage".format( error ) )
        print()
        sys.stdout.flush()
     
     
            
# Root process to create and save the plot
if rank == 0:
    # Final computation time elapsed
    t_f = time.time() - t_i
    
    print( "Computation time = {:.3f} s".format( t_f ) )
    sys.stdout.flush()
    

    # Plotting the data 
    if m_visual:
        
        # Save computation time in a json file
        if save_data:
            result = dict()
            result["comp_time"] = round( t_f, 3 )
            
            with open( result_file, "w" ) as r_f:
                r_f.write( json.dumps( result, indent=4 ) )
        
        #fig = plt.figure( figsize = ( 15, 7 ) )
        fig = plt.figure( figsize = ( 15, 7 ) )
        
        # Points plot
        ax_v = fig.add_subplot(121)
        ax_v.set( xlim=( -1.0, 1.0 ), ylim=( -1.0, 1.0 ) )
        
        ax_v.set_title( " Total points = {}\n Points in circle = {}\n Estimated \u03C0 \u2248 {:.7f}\n Time Elapsed = {:.1f} s".format( t_v_list[-1], t_c_in_list[-1], apprx_pi_list[-1], time_list[-1] ), loc = 'left', fontsize=12 )
        
        # For animation
        #ax_v.scatter( [], [], s=0.1, color='red', label="Points In" )
        #ax_v.scatter( [], [], s=0.1, color='blue', label="Points Out" )

        label_flag = True
        for i in range( no_of_batch ):
            if label_flag:
                label_flag = False
                ax_v.scatter( in_x_all[i], in_y_all[i], s=0.1, color='red', label="Points In" ) 
                ax_v.scatter( out_x_all[i], out_y_all[i], s=0.1, color='blue', label="Points Out" )
            else:
                ax_v.scatter( in_x_all[i], in_y_all[i], s=0.1, color='red' )
                ax_v.scatter( out_x_all[i], out_y_all[i], s=0.1, color='blue' )
        
        ax_v.legend( bbox_to_anchor=( 1, 1.10 ) )
        
        # Pi value comparison plot
        ax_pi = fig.add_subplot(122)
        ax_pi.set_xlabel( "No. of points" )
        ax_pi.set_ylabel( "Pi calculated" )
        ax_pi.set( ylim=( np.min(apprx_pi_list)-0.05, np.max(apprx_pi_list)+0.05 ) )
        ax_pi.set_title( "Pi value plot", fontsize=12 )
        
        pi_line, = ax_pi.plot( t_v_list, apprx_pi_list, color="teal", label="Approx Pi" )
        ax_pi.plot( t_v_list, np.zeros(no_of_batch)+math.pi, color="darkgray", label="Pi" )
        
        ax_pi.legend( loc="upper right" )
        
        # Animation of plot
        def animate( tt ):
            ax_v.set_title( " Total points = {}\n Points in circle = {}\n Estimated \u03C0 \u2248 {:.7f}\n Time Elapsed = {:.1f} s".format( t_v_list[tt], t_c_in_list[tt], apprx_pi_list[tt], time_list[tt] ), loc = 'left', fontsize=12 )            
            ax_v.scatter( in_x_all[tt], in_y_all[tt], s=0.1, color='red', label="Points In" )
            ax_v.scatter( out_x_all[tt], out_y_all[tt], s=0.1, color='blue', label="Points Out" )
            
            pi_line.set_data( t_v_list[:tt+1], apprx_pi_list[:tt+1] )
            
            return pi_line,
            
            
        #anim = animation.FuncAnimation( fig, animate, np.arange( 0, no_of_batch ), interval=400, repeat=False )
        
        
        # Save generated plot as specified file
        if save_data:
            #plt.show()
            #anim.save( vs_anim_file )
            fig.savefig( vs_final_file, bbox_inches='tight' )
        else:
            plt.show()
    
    elif m_strong_scale or m_weak_scale:        
        # Save the number of cores used along with its computation time and data size
        with open( sc_file, "a" ) as f:
            f.write( "{},{},{}\n".format( size, round( t_f, 3 ), total_v ) )
    
            
        
# End
