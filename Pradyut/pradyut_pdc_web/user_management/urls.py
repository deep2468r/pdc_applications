from django.contrib import admin

#from django.urls import path, include, re_path
from django.conf.urls import __path__, url, include

from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth import views as auth_views
from users.views import CustomLoginView, ResetPasswordView, ChangePasswordView

from users.forms import LoginForm

urlpatterns = [
    url('admin/', admin.site.urls),

    url('', include('users.urls')),

    url('login/', CustomLoginView.as_view(redirect_authenticated_user=True, template_name='users/login.html',
                                           authentication_form=LoginForm), name='login'),

    url('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),

    url('password-reset/', ResetPasswordView.as_view(), name='password_reset'),

    url('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'),
         name='password_reset_confirm'),

    url('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'),
         name='password_reset_complete'),

    url('password-change/', ChangePasswordView.as_view(), name='password_change'),

    url(r'^oauth/', include('social_django.urls', namespace='social')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
