#from random import choices
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from .models import Profile


class RegisterForm(UserCreationForm):
    # fields we want to include and customize in our form
    first_name = forms.CharField(max_length=100,
                                 required=True,
                                 widget=forms.TextInput(attrs={'placeholder': 'First Name',
                                                               'class': 'form-control',
                                                               }))
    last_name = forms.CharField(max_length=100,
                                required=True,
                                widget=forms.TextInput(attrs={'placeholder': 'Last Name',
                                                              'class': 'form-control',
                                                              }))
    username = forms.CharField(max_length=100,
                               required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Username',
                                                             'class': 'form-control',
                                                             }))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'placeholder': 'Email',
                                                           'class': 'form-control',
                                                           }))
    password1 = forms.CharField(max_length=50,
                                required=True,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                  'class': 'form-control',
                                                                  'data-toggle': 'password',
                                                                  'id': 'password',
                                                                  }))
    password2 = forms.CharField(max_length=50,
                                required=True,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password',
                                                                  'class': 'form-control',
                                                                  'data-toggle': 'password',
                                                                  'id': 'password',
                                                                  }))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']


class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=100,
                               required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Username',
                                                             'class': 'form-control',
                                                             }))
    password = forms.CharField(max_length=50,
                               required=True,
                               widget=forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                 'class': 'form-control',
                                                                 'data-toggle': 'password',
                                                                 'id': 'password',
                                                                 'name': 'password',
                                                                 }))
    remember_me = forms.BooleanField(required=False)

    class Meta:
        model = User
        fields = ['username', 'password', 'remember_me']


class UpdateUserForm(forms.ModelForm):
    username = forms.CharField(max_length=100,
                               required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['username', 'email']


class UpdateProfileForm(forms.ModelForm):
    avatar = forms.ImageField(widget=forms.FileInput(attrs={'class': 'form-control-file'}))
    bio = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': 5}))

    class Meta:
        model = Profile
        fields = ['avatar', 'bio']



n_choices = ( 
    ( "1", "1" ),
    ( "2", "2" ),
    ( "3", "3" ),
    ( "4", "4" ), )

p_choices = (
    ( "19", "2^19" ),
    ( "18", "2^18" ),
    ( "17", "2^17" ),
    ( "16", "2^16" ),
    ( "15", "2^15" ),
    ( "14", "2^14" ),
)

class PiEstForm( forms.Form ):
    job_name = forms.CharField( max_length=100 )
    nodes = forms.ChoiceField( choices = n_choices )
    ntasks_per_node = forms.ChoiceField( choices = n_choices )
    num_points_pow_2 = forms.ChoiceField( choices = p_choices )
    


gs_choices = (
    ( "32", "32" ),
    ( "64", "64" ),
    ( "128", "128" ),
)

source_choices = (
    ( "20", "20" ),
    ( "40", "40" ),
    ( "60", "60" ),
    ( "80", "80" ),
    ( "100", "100" ),
    ( "140", "140" ),
    ( "180", "180" ),
    ( "200", "200" ),
)

sink_choices = (
    ( "-20", "-20" ),
    ( "-40", "-40" ),
    ( "-60", "-60" ),
    ( "-80", "-80" ),
    ( "-100", "-100" ),
    ( "-140", "-140" ),
    ( "-180", "-180" ),
    ( "-200", "-200" ),
)

tolr_choices = (
    ( "0.1", "1e-1" ),
    ( "0.01", "1e-2" ),
    ( "0.001", "1e-3" ),
    ( "0.0001", "1e-4" ),
    ( "0.00001", "1e-5" ),
)


class HeatDiffForm( forms.Form ):
    job_name = forms.CharField( max_length=100 )
    nodes = forms.ChoiceField( choices = n_choices )
    ntasks_per_node = forms.ChoiceField( choices = n_choices )
    grid_size = forms.ChoiceField( choices = gs_choices )
    source_temp = forms.ChoiceField( choices = source_choices )
    sink_temp = forms.ChoiceField( choices = sink_choices )
    tolerance = forms.ChoiceField( choices = tolr_choices )
