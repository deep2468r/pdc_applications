#from django.urls import path, re_path
from django.conf.urls import url

from .views import home, profile, RegisterView, pi_est, heat_diff
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic import TemplateView

urlpatterns = [
    url('', home, name='users-home'),
    url('register/', RegisterView.as_view(), name='users-register'),
    url('profile/', profile, name='users-profile'),
    url('pi_estimate/', TemplateView.as_view( template_name = "users/pi_estimate.html" ), name='pi_estimate' ),
    url('pi_estimate_output/', pi_est, name='pi_estimate_output'),
    url('heat_diffusion/', TemplateView.as_view( template_name = 'users/heat_diffusion.html' ), name='heat_diffusion' ),
    url('heat_diffusion_output/', heat_diff, name='heat_diffusion_output'),
    url('lorentz_force_output/', TemplateView.as_view( template_name = 'users/lorentz_force_output.html' ), name='lorentz_force_output' ),
]

urlpatterns += staticfiles_urlpatterns()
