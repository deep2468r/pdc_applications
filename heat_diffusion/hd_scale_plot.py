# Heat Diffusion
# Create scale plot from data stored in hd_core_time.csv

import csv
from matplotlib import pyplot as plt
import json
from os import environ


# To handle matplotlib warnings
def suppress_qt_warnings():
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"
    
    


# Main

# Flags/Operation parameters
with open( "hd_input.json", "r" ) as in_f:
    d_params = json.load( in_f )

grid_size = d_params["grid_size"]
iterations = d_params["iterations"]

if d_params["mode"] == "strong_scale":
    m_sc_strong = True
    m_sc_weak = False
elif d_params["mode"] == "weak_scale":
    m_sc_strong = False
    m_sc_weak = True



# Files
if m_sc_strong:
    sc_file = "hd_core_time_strong.csv"
    sc_plt_file = "hd_scale_strong.png"
elif m_sc_weak:
    sc_file = "hd_core_time_weak.csv"
    sc_plt_file = "hd_scale_weak.png"
    

# Lists to store data
# Number of cores utilised
core_list = []
# Ideal speed-up
ideal_list = []
# Actual speed-up
real_list = []

# Dictionary to store computation time for number of cores used
d_ct = dict()

# Reading data from csv file and storing the data
with open( sc_file, "r" ) as ct_f:
    csv_rdr = csv.reader( ct_f )
    
    for v in csv_rdr:
        c = int( v[0] )
        t = float( v[1] ) 
        core_list.append( c )
        d_ct[c] = t


core_list.sort()
base_core = core_list[0] 
base_time = d_ct[base_core]

for i in range( len(core_list) ):
    
    if m_sc_strong:
        ideal_list.append( core_list[i]//base_core )
    elif m_sc_weak:
        ideal_list.append( base_core//base_core )
    
    real_list.append( base_time/d_ct[ core_list[i] ] )
        
        
         
suppress_qt_warnings()

# Plotting
plt.figure( figsize=( 8.5, 6 ) )

plt.xscale("log", base=2)
plt.xticks( core_list, core_list )

if m_sc_strong:
    plt.yscale("log", base=2)
    plt.yticks( ideal_list, ideal_list )
elif m_sc_weak:
    plt.ylim( 0.0, 1.1 )
    
plt.plot( core_list, ideal_list, marker='x', color='darkgray', label='Ideal scale' )
plt.plot( core_list, real_list, marker='o', color='teal', label='Actual scale' )

#plt.legend(loc='best')
plt.legend( bbox_to_anchor=( 1.01, 1.12 ) )
plt.grid( color='lightgray', linestyle='-' )
plt.xlabel("No. of cores")
plt.ylabel("Speed-up")

if m_sc_strong:
    plt.title("Heat Diffusion Strong Scale\nGrid size = {}\nIterations = {}".format( grid_size, iterations ), loc="left", fontsize=17 )
elif m_sc_weak:
    plt.title("Heat Diffusion Weak Scale\nIterations = {}".format( iterations ), loc="left", fontsize=17 )
    

plt.savefig( sc_plt_file, bbox_inches='tight' )
#plt.show()