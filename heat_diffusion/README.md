
# **Heat Diffusion**    

<br>

---

## **Introduction**

---

To display the transfer of heat from hot area to cold area in a 2D plane. The plane consists of a heat source and a heat sink.

---





<br>
<br>
<br>

---

## **Files**

---

- ***heat_diff.py*** :- Main Python file containing heat diffusion code.
- ***hd_scale_plot.py*** :- Python file to create scale plot from data stored in *hd_core_time_strong.csv* or *hd_core_time_weak.csv*     
- ***hd_input.json*** :- JSON file to specify mode of operation and other parameters.
- ***hd_core_time_strong.csv*** :- CSV file to store number of cores and their respective computation time and data size for strong scaling.    
- ***hd_core_time_weak.csv*** :- CSV file to store number of cores and their respective computation time and data size for weak scaling.       






<br>
<br>
<br>

---
    
## **Input parameters**

---

### **hd_input.json**
> *{   
"mode": "visual",     
"grid_size": 64,  
"iterations": 16000,  
"interval": 8,      
"source_temp": 100.0,     
"sink_temp": -40.0,      
"color_map": "jet",   
"tolerance": 1e-2,
"save_data": true
}*

---

**mode** :- Mode of operation. Values = ( basic, visual, strong_scale, weak_scale )
> *basic* = To simply display output on command line.     
> *visual* = To perform visualization.     
> *strong_scale* = To do strong scaling.     
> *weak_scale* = To do weak scaling.      

**grid_size** :- Size of the plane matrix in which diffusion will take place.     

**iterations** :- No. of iterations to perform calculations  of temperature data for diffusion in the plane.      

**interval** :- Update visualization plot after every *n* iterations.

**source_temp** :- Temperature of heat source.      

**sink_temp** :- Temperature of heat sink.      

**color_map** :- Choose colour map for visualization.     
> Preferred options : *jet*, *hot*, *turbo*, *rainbow*, *inferno*

**tolerance** :- To iterate until the difference between old and new temperature of a point is greater than the tolerance value.     

**save_data** :- Whether to save the plot when visual mode is set. Not utilised for other modes. Values = ( true, false )     

---





<br>
<br>
<br>

---

## **Basic**

---

It will do just calculations and display final result on command line. 

1. Set ***"mode":"basic"*** in ***hd_input.json*** file.           
2. You can change other parameters as per your preference.             
3. Execute ***antya_hd_basic.sh*** using PBS.  
              
> *$qsub antya_hd_basic.sh*           

---



<br>
<br>
<br>

---

## **Visualization**

---

It will create a plot from temperature data being calculated in the plane matrix.  

1. Set ***"mode":"visual"*** in ***hd_input.json*** file.               
2. You can change other parameters as per your preference.              
3. Execute ***antya_hd_visual.sh*** using PBS.              

> *$qsub antya_hd_visual.sh*    
               
4. After completion, the plot will be saved as ***hd_visual_anim.gif*** and ***hd_visual_final.png***.      

---



<br>
<br>
<br>


---

## **Scale**

---

To scale the performance of heat diffusion code for different number of cores.    

1. For strong scaling set ***"mode":"strong_scale"***, ***"grid_size": 64*** and ***"iterations": 1000*** in ***hd_input.json*** file. For weak scaling set ***"mode":"weak_scale"*** and ***"iterations": 2000*** in ***hd_input.json*** file.              
2. Above mentioned parameters can be changed as per users preference for each scaling. Other parameters are not considered.                 
3. Execute ***antya_hd_scale.sh*** using PBS.              

> *$qsub antya_hd_scale.sh*         
        
4. After completion, the scale plot will be saved as ***hd_scale_strong.png*** for strong scaling or as ***hd_scale_weak.png*** for weak scaling.               

---




<br>
<br>
<br>

---

### **Note :-**

---

- On changing ***grid_size*** make sure that,  
  
> ( number of cores ) x 8 <= *grid_size*          

- For good visualization, set the parameters for any grid_size as shown in ***hd_input_preferences.txt*** file.                           
- To change the number of cores you have to edit the following lines in the ***.sh*** file for the respective mode,               

> *$#PBS -l select=2:ncpus=32*      
    
> *$mpirun -n 64 python heat_diff.py*             
                 