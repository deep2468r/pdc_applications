#!/bin/bash

#SBATCH --job-name=heat_diff_scale_weak
#SBATCH --partition=pradyut
#SBATCH --output=heat_diff_scale_weak_%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4

source activate test_env



# Delete core_time.csv if it already exists and create a new one
csv_file=hd_core_time_weak.csv
if test -f "$csv_file"
then
    rm $csv_file
fi


# Cores list
c_l=(1 2 4 8)

# Running code for different no. of cores
printf "Heat Diffusion Weak Scaling\n"
printf "Processing for...\n"

for i in ${c_l[*]}
do 
    mpirun -n $i python heat_diff.py
done

#wait

printf "\n\nProcessing completed.\n"


# Plotting the speed-up graph
printf "\nPlotting\n"

python hd_scale_plot.py

printf "Plot file saved.\n"
printf "\nEnd\n"