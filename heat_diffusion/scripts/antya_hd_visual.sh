#!/bin/bash

#PBS -N heat_diff_visual
#PBS -q extq2

#PBS -l select=2:ncpus=32

#PBS -l walltime=24:00:00
#PBS -j oe


cd $PBS_O_WORKDIR

module load anaconda/3
module load openmpi/gcc/4.0.1

source /home/application/anaconda3V2/etc/profile.d/conda.sh

conda activate dist_env



mpirun -n 64 python heat_diff.py 
