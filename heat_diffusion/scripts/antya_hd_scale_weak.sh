#!/bin/bash

#PBS -N heat_diff_scale_weak
#PBS -q extq2

#PBS -l select=13:ncpus=40

#PBS -l walltime=24:00:00
#PBS -j oe


cd $PBS_O_WORKDIR

module load anaconda/3
module load openmpi/gcc/4.0.1

source /home/application/anaconda3V2/etc/profile.d/conda.sh

conda activate dist_env

csv_file=hd_core_time_weak.csv
if test -f "$csv_file"
then
    rm $csv_file
fi



# Cores list
c_l=(4 8 16 32 64 128 256)

# Running code for different no. of cores
printf "Heat Diffusion Weak Scaling\n"
printf "Processing for...\n"

for i in ${c_l[*]}
do 
    mpirun -n $i python heat_diff.py &
done

wait

printf "\n\nProcessing completed.\n"


# Plotting the speed-up graph
printf "\nPlotting\n"

python hd_scale_plot.py

printf "Plot file saved.\n"
printf "\nEnd\n"
