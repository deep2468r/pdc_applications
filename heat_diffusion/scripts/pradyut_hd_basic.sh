#!/bin/bash

#SBATCH --job-name=heat_diff_basic
#SBATCH --partition=pradyut
#SBATCH --output=heat_diff_basic_%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4

source activate test_env

mpirun -n 8 python heat_diff.py