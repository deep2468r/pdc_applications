from threading import local
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordChangeView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.views import View
from django.contrib.auth.decorators import login_required

from .forms import RegisterForm, LoginForm, UpdateUserForm, UpdateProfileForm, PiEstForm, HeatDiffForm

import os
import json
from pathlib import Path
import time



def home(request):
    return render(request, 'users/home.html')


class RegisterView(View):
    form_class = RegisterForm
    initial = {'key': 'value'}
    template_name = 'users/register.html'

    def dispatch(self, request, *args, **kwargs):
        # will redirect to the home page if a user tries to access the register page while logged in
        if request.user.is_authenticated:
            return redirect(to='/')

        # else process dispatch as it otherwise normally would
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()

            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}')

            return redirect(to='login')

        return render(request, self.template_name, {'form': form})



class CustomLoginView(LoginView):
    form_class = LoginForm

    def form_valid(self, form):
        remember_me = form.cleaned_data.get('remember_me')

        if not remember_me:
            self.request.session.set_expiry(0)

            self.request.session.modified = True

        return super(CustomLoginView, self).form_valid(form)


class ResetPasswordView(SuccessMessageMixin, PasswordResetView):
    template_name = 'users/password_reset.html'
    email_template_name = 'users/password_reset_email.html'
    subject_template_name = 'users/password_reset_subject'
    success_message = "We've emailed you instructions for setting your password, " \
                      "if an account exists with the email you entered. You should receive them shortly." \
                      " If you don't receive an email, " \
                      "please make sure you've entered the address you registered with, and check your spam folder."
    success_url = reverse_lazy('users-home')


class ChangePasswordView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'users/change_password.html'
    success_message = "Successfully Changed Your Password"
    success_url = reverse_lazy('users-home')


@login_required
def profile(request):
    if request.method == 'POST':
        user_form = UpdateUserForm(request.POST, instance=request.user)
        profile_form = UpdateProfileForm(request.POST, request.FILES, instance=request.user.profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Your profile is updated successfully')
            return redirect(to='users-profile')
    else:
        user_form = UpdateUserForm(instance=request.user)
        profile_form = UpdateProfileForm(instance=request.user.profile)

    return render(request, 'users/profile.html', {'user_form': user_form, 'profile_form': profile_form})


# Pi Estimate
def pi_est(request):

    job_name = ""
    nodes = 0
    ntasks_per_node = 0
    num_points_pow_2 = 0

    comp_time = 0


    # Session
    if request.session.has_key('pe'):
        request.session['pe'] += 1

        if request.session['pe'] == 21:
            request.session['pe'] = 1

    else:
        request.session['pe'] = 1
    
    pe_out = request.session['pe']


    # Process form data and execute job
    if request.method == 'POST':
        PiEstData = PiEstForm( request.POST )

        if PiEstData.is_valid():
            job_name = PiEstData.cleaned_data['job_name']
            nodes = PiEstData.cleaned_data['nodes']
            ntasks_per_node = PiEstData.cleaned_data['ntasks_per_node']
            num_points_pow_2 = PiEstData.cleaned_data['num_points_pow_2']
            

            # Code directory path
            code_path = "/kuberadir/share/pdc_code/web/pi_estimate"
            

            # Static output directory path
            out_static_path = "/home/deepakagg/PDC/web/server_pdc_web/users/static/anim/pi_estimate/" + str( pe_out )
            hosts_path = "/home/deepakagg/PDC/web/server_pdc_web/users/static/hosts"

            if not os.path.isdir( out_static_path ):
                os.system( "mkdir " + out_static_path )


            # Code session directory path
            code_sess_path = os.path.join( code_path, str(pe_out) )


            # Creating script file
            tasks = str( int(nodes)*int(ntasks_per_node) )

            ss_data = "#!/bin/bash\n" + "\n#SBATCH --job-name=" + job_name + "\n#SBATCH --partition=pradyut" + "\n#SBATCH --output=" + job_name + "_%j.out" + "\n#SBATCH --nodes=" + str(nodes) + "\n#SBATCH --ntasks-per-node=" + str(ntasks_per_node) + "\n\n"
            ss_data += "export PATH=/kuberadir/share/miniconda3/bin:$PATH \n export PATH=/kuberadir/share/installed-software/mpich-3.1/bin:$PATH \n\n"
            ss_data += "mpirun -f " + os.path.join( code_sess_path, "hosts" ) + " -n " + tasks + " python " + os.path.join( code_path, "pi_est.py" ) + " " + str( pe_out )

            ss_f_path = os.path.join( out_static_path, "pe_visual.sh" )
            pr_ss_f_path = os.path.join( code_sess_path, "pe_visual.sh" )

            with open( ss_f_path, "w") as ss_f:
                ss_f.write(ss_data)
                
            

            # Creating input file
            in_d = dict()
            in_d["mode"] = "visual"
            in_d["num_points_pow_2"] = int(num_points_pow_2)
            in_d["no_of_batch"] = 16
            in_d["save_data"] = True
            

            in_f_path = os.path.join( out_static_path, "pe_input.json" )

            with open( in_f_path, "w" ) as in_f:
                in_f.write( json.dumps( in_d, indent=4 ) )
                
            
            os.system( "cp " + hosts_path + " " + out_static_path )
            os.system( "scp -r " + out_static_path + " pi@10.20.4.65:" + code_path )
            
            

            # Run job
            os.system( "ssh pi@10.20.4.65 'chmod 774 " + pr_ss_f_path + "'" )
            os.system( "ssh pi@10.20.4.65 '" + pr_ss_f_path + "'" )

            # Copy output to static folder
            pr_out_path = os.path.join( code_sess_path, "pe_visual_final.png" )

            os.system( "scp pi@10.20.4.65:" + pr_out_path + " " + out_static_path )
            

            # Read computation time
            pr_res_f_path = os.path.join( code_sess_path, "pe_result.json" )
            os.system( "scp pi@10.20.4.65:" + pr_res_f_path + " " + out_static_path )
            res_f_path = os.path.join( out_static_path, "pe_result.json" )


            with open( res_f_path, "r" ) as res_f:
                res_d = json.load( res_f )

            comp_time = res_d["comp_time"]

            num_points = pow( 2, int(num_points_pow_2) )

    
    return render( request, 'users/pi_estimate_output.html', locals() )




# Heat Diffusion
def heat_diff(request):
    
    job_name = ""
    nodes = 0
    ntasks_per_node = 0
    grid_size = 0
    source_temp = 0
    sink_temp = 0
    tolerance = 0

    comp_time = 0


    # Session
    if request.session.has_key('hd'):
        request.session['hd'] += 1

        if request.session['hd'] == 21:
            request.session['hd'] = 1

    else:
        request.session['hd'] = 1
    
    hd_out = request.session['hd']


    # Process form data and execute job
    if request.method == 'POST':
        HeatDiffData = HeatDiffForm( request.POST )
        
        if HeatDiffData.is_valid():
            job_name = HeatDiffData.cleaned_data['job_name']
            nodes = HeatDiffData.cleaned_data['nodes']
            ntasks_per_node = HeatDiffData.cleaned_data['ntasks_per_node']
            grid_size = HeatDiffData.cleaned_data['grid_size']
            source_temp = HeatDiffData.cleaned_data['source_temp']
            sink_temp = HeatDiffData.cleaned_data['sink_temp']
            tolerance = HeatDiffData.cleaned_data['tolerance']
            

            # Code directory path
            code_path = "/kuberadir/share/pdc_code/web/heat_diffusion"
            

            # Static output directory path
            out_static_path = "/home/deepakagg/PDC/web/server_pdc_web/users/static/anim/heat_diffusion/" + str( hd_out )
            hosts_path = "/home/deepakagg/PDC/web/server_pdc_web/users/static/hosts"

            if not os.path.isdir( out_static_path ):
                os.system( "mkdir " + out_static_path )


            # Code session directory path
            code_sess_path = os.path.join( code_path, str(hd_out) )


            # Creating script file            
            tasks = str( int(nodes)*int(ntasks_per_node) )

            ss_data = "#!/bin/bash\n" + "\n#SBATCH --job-name=" + job_name + "\n#SBATCH --partition=pradyut" + "\n#SBATCH --output=" + job_name + "_%j.out" + "\n#SBATCH --nodes=" + str(nodes) + "\n#SBATCH --ntasks-per-node=" + str(ntasks_per_node) + "\n\n"
            ss_data += "export PATH=/kuberadir/share/miniconda3/bin:$PATH \n export PATH=/kuberadir/share/installed-software/mpich-3.1/bin:$PATH \n\n"
            ss_data += "mpirun -f " + os.path.join( code_sess_path, "hosts" ) + " -n " + tasks + " python " + os.path.join( code_path, "heat_diff.py" ) + " " + str( hd_out )
    
            ss_f_path = os.path.join( out_static_path, "hd_visual.sh" )
            pr_ss_f_path = os.path.join( code_sess_path, "hd_visual.sh" )

            with open( ss_f_path, "w") as ss_f:
                ss_f.write(ss_data)
                
            
            
            # Creating input file
            in_d = dict()
            in_d["mode"] = "visual" 
            in_d["grid_size"] = int(grid_size)
            
            if grid_size == "32":
                in_d["iterations"] = 4000
                in_d["interval"] = 2
            elif grid_size == "64":
                in_d["iterations"] = 16000
                in_d["interval"] = 8
            elif grid_size == "128":
                in_d["iterations"] = 64000
                in_d["interval"] = 32
            
            in_d["source_temp"] = int(source_temp)
            in_d["sink_temp"] = int(sink_temp)
            in_d["color_map"] = "jet"
            in_d["tolerance"] = float(tolerance)
            in_d["save_data"] = True
            
            in_f_path = os.path.join( out_static_path, "hd_input.json" )

            with open( in_f_path, "w" ) as in_f:
                in_f.write( json.dumps( in_d, indent=4 ) )
                
            
            os.system( "cp " + hosts_path + " " + out_static_path )
            os.system( "scp -r " + out_static_path + " pi@10.20.4.65:" + code_path )
            
            
            
            # Run job
            os.system( "ssh pi@10.20.4.65 'chmod 774 " + pr_ss_f_path + "'" )
            os.system( "ssh pi@10.20.4.65 '" + pr_ss_f_path + "'" )

            # Copy output to static folder
            pr_out_path = os.path.join( code_sess_path, "hd_visual_final.png" )
            
            os.system( "scp pi@10.20.4.65:" + pr_out_path + " " + out_static_path )


            # Read computation time
            pr_res_f_path = os.path.join( code_sess_path, "hd_result.json" )
            os.system( "scp pi@10.20.4.65:" + pr_res_f_path + " " + out_static_path )
            res_f_path = os.path.join( out_static_path, "hd_result.json" )

            with open( res_f_path, "r" ) as res_f:
                res_d = json.load( res_f )

            comp_time = res_d["comp_time"]

            
    
    return render( request, 'users/heat_diffusion_output.html', locals() )
    