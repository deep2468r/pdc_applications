
# Antya Anaconda Setup

### Steps :-

1. Create a conda environment named ***dist_env***.                     
> *$conda create --name dist_env*           

2. Switch to ***dist_env*** environment.                
> *$conda activate dist_env*               

3. Install ***python 3.7.7***                  
> *$conda install python=3.7.7*                    

4. Install ***matplotlib*** and ***mpi4py***.                    
> *$conda install matplotlib mpi4py*              

