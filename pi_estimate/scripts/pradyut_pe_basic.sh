#!/bin/bash

#SBATCH --job-name=pi_est_basic
#SBATCH --partition=pradyut
#SBATCH --output=pi_est_basic_%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4

source activate test_env

mpirun -n 8 python pi_est.py