#!/bin/bash

#PBS -N pi_est_scale_strong
#PBS -q extq2

#PBS -l select=13:ncpus=40

#PBS -l walltime=24:00:00
#PBS -j oe


cd $PBS_O_WORKDIR

module load anaconda/3
module load openmpi/gcc/4.0.1

source /home/application/anaconda3V2/etc/profile.d/conda.sh

conda activate dist_env

csv_file=pi_core_time_strong.csv
if test -f "$csv_file"
then
    rm $csv_file
fi



# Cores list
c_l=(1 2 4 8 16 32 64 128 256)

# Running code for different no. of cores
printf "Pi Estimate Strong Scaling\n"
printf "Processing for...\n"

for i in ${c_l[*]}
do 
    mpirun -n $i python pi_est.py &
done

wait

printf "\n\nProcessing completed.\n"


# Plotting the speed-up graph
printf "\nPlotting\n"

python pe_scale_plot.py

printf "Plot file saved.\n"
printf "\nEnd\n"
