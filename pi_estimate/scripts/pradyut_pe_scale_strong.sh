#!/bin/bash

#SBATCH --job-name=pi_est_scale_strong
#SBATCH --partition=pradyut
#SBATCH --output=pi_est_scale_strong_%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4

source activate test_env



# Delete pe_core_time_strong.csv if it already exists and create a new one
csv_file=pe_core_time_strong.csv
if test -f "$csv_file"
then
    rm $csv_file
fi


# Cores list
c_l=(1 2 4 8)

# Running code for different no. of cores
printf "Pi Estimate Strong Scaling\n"
printf "Processing for...\n"

for i in ${c_l[*]}
do 
    mpirun -n $i python pi_est.py
done

#wait

printf "\n\nProcessing completed.\n"


# Plotting the speed-up graph
printf "\nPlotting\n"

python pe_scale_plot.py

printf "Plot file saved.\n"
printf "\nEnd\n"