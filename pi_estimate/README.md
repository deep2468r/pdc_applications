
# **Pi Estimate**    

<br>

---

## **Introduction**

---

To estimate the value of PI approximately using the Monte Carlo approach. This method consists of drawing on a canvas a square with an inner circle. We then generate a large number of random points within the square and count how many fall in the enclosed circle.    

> Area of circle = πr2    
Area of square = (2r)2 = 4r2    
Ratio of area of circle to square = π/4    

The same ratio can be used between the number of points within the square and number of points within the circle.    

Therefore we can use following to estimate value of Pi,    
>π = 4 x ( number of points in circle / total number of points )

---





<br>
<br>
<br>

---

## **Files**

---

- ***pi_est.py*** :- Main Python file containing pi estimate code.
- ***pe_scale_plot.py*** :- Python file to create scale plot from data stored in *pe_core_time_strong.csv* or *pe_core_time_weak.csv*.     
- ***pe_input.json*** :- JSON file to specify mode of operation and other parameters.
- ***pe_core_time_strong.csv*** :- CSV file to store store number of cores and their respective computation time and data size for strong scaling.           
- ***pe_core_time_weak.csv*** :- CSV file to store store number of cores and their respective computation time and data size for weak scaling. 





<br>
<br>
<br>

---
    
## **Input parameters**

---

### **pe_input.json**
> *{   
"mode": "visual",  
"num_points_pow_2": 26, 
"no_of_batch": 16,
"save_data": true   
}*

---

**mode** :- Mode of operation. Values = ( basic, visual, strong_scale, weak_scale )     
> *basic* = To simply display output on command line.    
> *visual* = To perform visualization.     
> *strong_scale* = To do strong scaling.    
> *weak_scale* = To do weak scaling.     

**num_points_pow_2** :- Power of 2 which represents the number of points to generate.

**no_of_batch** :- Number of batches to divide and display the data in plot.

**save_data** :- Whether to save the plot when visual mode is set. Not utilised for other modes. Values = ( true, false ) 





<br>
<br>
<br>

---

## **Basic**

---

It will simply display the output calculated Pi value on command line.  

1. Set ***"mode":"basic"*** in ***pe_input.json*** file.    
2. Execute ***antya_pe_basic.sh*** using PBS.            

> *$qsub antya_pe_visual.sh*    

---






<br>
<br>
<br>

---

## **Visualization**

---

It will create a plot displaying points being generated inside and outside of circle on the left side and Pi value plot on the right side. 

1. Set ***"mode":"visual"*** in ***pe_input.json*** file.    
2. Execute ***antya_pe_visual.sh*** using PBS.            

> *$qsub antya_pe_visual.sh*    

3. After completion, the plot will be displayed or if the ***"save_data": true*** is set then the plot will be saved as ***pe_visual_anim.gif*** and ***pe_visual_final.png***.

---







<br>
<br>
<br>

---

## **Scale**

---

To scale the performance of pi estimate code for different number of cores.    

1. Set ***"mode":"strong_scale"*** for strong scaling or set ***"mode":"weak_scale"*** for weak scaling in ***pe_input.json*** file.        
2. Execute ***antya_pe_scale.sh*** using PBS.                       

> *$qsub antya_pe_scale.sh*  

3. After completion, the scale plot will be saved as ***pe_scale_strong.png*** for strong scaling or as ***pe_scale_weak.png*** for weak scaling. 

---
