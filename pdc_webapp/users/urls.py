from django.urls import path
from .views import home, profile, RegisterView, pi_est, heat_diff
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.views.generic import TemplateView

urlpatterns = [
    path('', home, name='users-home'),
    path('register/', RegisterView.as_view(), name='users-register'),
    path('profile/', profile, name='users-profile'),
    path('pi_estimate/', TemplateView.as_view( template_name = "users/pi_estimate.html" ), name='pi_estimate' ),
    path('pi_estimate_output/', pi_est, name='pi_estimate_output'),
    path('heat_diffusion/', TemplateView.as_view( template_name = 'users/heat_diffusion.html' ), name='heat_diffusion' ),
    path('heat_diffusion_output/', heat_diff, name='heat_diffusion_output'),
]

urlpatterns += staticfiles_urlpatterns()
